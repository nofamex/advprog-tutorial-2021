package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaServiceImpl;
import csui.advpro2021.tais.service.PaymentServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(controllers = LogController.class)
public class LogControlletTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private MahasiswaServiceImpl mahasiswaService;

    @MockBean
    private PaymentServiceImpl paymentService;

    private Mahasiswa mahasiswa;

    private Log log;
}
