package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name = "payment")
@Data
@NoArgsConstructor
public class Payment {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, unique = true)
    @JsonIgnore
    private Integer id;

    @Column(name = "bulan", updatable = false, nullable = false)
    private String bulan;

    @Column(name = "total_jam")
    private Long totalJam;

    @Column(name = "pembayaran")
    private Long pembayaran;

    @ManyToOne()
    @JoinColumn(name = "npm")
    @JsonIgnore
    private Mahasiswa mahasiswa;

    public Payment(String bulan, Long totalJam, Long pembayaran) {
        this.bulan = bulan;
        this.totalJam = totalJam;
        this.pembayaran = pembayaran;
    }
}
