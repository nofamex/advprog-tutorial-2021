package csui.advpro2021.tais.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.Calendar;
import java.util.Date;

@Entity
@Table(name = "log")
@Data
@NoArgsConstructor
public class Log {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, unique = true)
    private Integer id;

    @ManyToOne()
    @JoinColumn(name = "npm")
    @JsonIgnore
    private Mahasiswa mahasiswa;

    @Column(name = "start_time")
    private Calendar start;

    @Column(name = "end_time")
    private Calendar end;

    @Column(name = "deksripsi")
    private String deskripsi;

    public Log(Calendar start, Calendar end, String deskripsi) {
        this.start = start;
        this.end = end;
        this.deskripsi = deskripsi;
    }
}
