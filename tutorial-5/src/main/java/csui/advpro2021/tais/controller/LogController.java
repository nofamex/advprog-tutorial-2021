package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.LogService;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/log")
public class LogController {
    @Autowired
    private LogService logService;

    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private PaymentService paymentService;

    @PostMapping(path = "/{npm}",produces = {"application/json"})
    @ResponseBody
    public ResponseEntity postLog(@PathVariable(value = "npm") String npm,@RequestBody Log log) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null || mahasiswa.getMataKuliah() == null) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        paymentService.addPayment(log.getStart(), log.getEnd(), mahasiswa);
        return ResponseEntity.ok(logService.createLog(log, mahasiswa));
    }

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getAllLog(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        return ResponseEntity.ok(logService.findAllLogByMahasiswa(mahasiswa));
    }

    @PutMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity editLog(@PathVariable(value = "id") Integer id, @RequestBody Log log) {
        Log existedLog = logService.findLogById(id);
        if (existedLog == null) {
            return new ResponseEntity(HttpStatus.FORBIDDEN);
        }
        return ResponseEntity.ok(logService.updateLog(id, log, existedLog.getMahasiswa()));
    }

    @DeleteMapping(path = "/{id}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity deleteLog(@PathVariable(value = "id") Integer id) {
        Log log = logService.deleteLog(id);
        if (log != null) {
            paymentService.removePayment(log.getStart(), log.getEnd(), log.getMahasiswa());
        }
        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
