package csui.advpro2021.tais.controller;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.service.MahasiswaService;
import csui.advpro2021.tais.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/payment")
public class PaymentController {
    @Autowired
    private MahasiswaService mahasiswaService;

    @Autowired
    private PaymentService paymentService;

    @GetMapping(path = "/{npm}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getAllMahasiswaPayment(@PathVariable(value = "npm") String npm) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(paymentService.findMahasiswaPayment(mahasiswa));
    }

    @GetMapping(path = "/{npm}/{bulan}", produces = {"application/json"})
    @ResponseBody
    public ResponseEntity getMahasiswaPaymentByBulan(@PathVariable(value = "npm") String npm, @PathVariable(value = "bulan") String bulan) {
        Mahasiswa mahasiswa = mahasiswaService.getMahasiswaByNPM(npm);
        if (mahasiswa == null) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
        return ResponseEntity.ok(paymentService.findMahasiswaPaymentByBulan(mahasiswa, bulan));
    }
}
