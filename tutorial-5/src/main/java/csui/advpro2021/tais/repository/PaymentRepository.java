package csui.advpro2021.tais.repository;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Payment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PaymentRepository extends JpaRepository<Payment, Integer> {
    Payment findByMahasiswaAndBulan(Mahasiswa mahasiswa, String bulan);
    Iterable<Payment> findAllByMahasiswa(Mahasiswa mahasiswa);
}
