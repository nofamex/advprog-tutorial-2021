package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Payment;

import java.util.Calendar;

public interface PaymentService {
    void addPayment(Calendar start, Calendar end, Mahasiswa mahasiswa);

    void removePayment(Calendar start, Calendar end, Mahasiswa mahasiswa);

    Iterable<Payment> findMahasiswaPayment(Mahasiswa mahasiswa);

    Payment findMahasiswaPaymentByBulan(Mahasiswa mahasiswa, String bulan);
}
