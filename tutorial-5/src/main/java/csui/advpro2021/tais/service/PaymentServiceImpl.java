package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.model.Payment;
import csui.advpro2021.tais.repository.PaymentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DateFormatSymbols;
import java.util.Calendar;

@Service
public class PaymentServiceImpl implements PaymentService{
    @Autowired
    private PaymentRepository paymentRepository;

    @Override
    public void addPayment(Calendar start, Calendar end, Mahasiswa mahasiswa) {
        Long[] hourAndMonthDiff = this.getHourAndMonthDifferent(end.getTimeInMillis(), start.getTimeInMillis());
        String monthStartName = this.getMonthName(start.get(Calendar.MONTH));
        String monthEndName = this.getMonthName(end.get(Calendar.MONTH));

        Payment paymentStart = paymentRepository.findByMahasiswaAndBulan(mahasiswa, monthStartName);
        Payment paymentEnd = paymentRepository.findByMahasiswaAndBulan(mahasiswa, monthEndName);

        this.savePayment(paymentStart, monthStartName, hourAndMonthDiff[0], 350 * hourAndMonthDiff[0], mahasiswa);
        if (hourAndMonthDiff[1] > 0) {
            this.savePayment(paymentEnd, monthEndName, hourAndMonthDiff[0], 350 * hourAndMonthDiff[0], mahasiswa);
        }
    }

    @Override
    public void removePayment(Calendar start, Calendar end, Mahasiswa mahasiswa) {
        Long[] hourAndMonthDiff = this.getHourAndMonthDifferent(end.getTimeInMillis(), start.getTimeInMillis());
        String monthStartName = this.getMonthName(start.get(Calendar.MONTH));
        String monthEndName = this.getMonthName(end.get(Calendar.MONTH));

        Payment paymentStart = paymentRepository.findByMahasiswaAndBulan(mahasiswa, monthStartName);
        Payment paymentEnd = paymentRepository.findByMahasiswaAndBulan(mahasiswa, monthEndName);

        this.reducePayment(paymentStart, hourAndMonthDiff[0], 350 * hourAndMonthDiff[0]);
        if (hourAndMonthDiff[1] > 0) {
            this.reducePayment(paymentEnd, hourAndMonthDiff[0], 350 * hourAndMonthDiff[0]);
        }
    }

    @Override
    public Iterable<Payment> findMahasiswaPayment(Mahasiswa mahasiswa) {
        return paymentRepository.findAllByMahasiswa(mahasiswa);
    }

    @Override
    public Payment findMahasiswaPaymentByBulan(Mahasiswa mahasiswa, String bulan) {
        return paymentRepository.findByMahasiswaAndBulan(mahasiswa, bulan);
    }

    private void reducePayment(Payment payment, Long totalJam, Long pembayaran) {
        payment.setTotalJam(payment.getTotalJam() - totalJam);
        payment.setPembayaran(payment.getPembayaran() - pembayaran);
        paymentRepository.save(payment);

        if (payment.getTotalJam() == 0) {
            paymentRepository.delete(payment);
        }
    }

    private void savePayment(Payment payment, String month, Long totalJam, Long pembayaran, Mahasiswa mahasiswa) {
        if (payment == null) {
            Payment newPayment = new Payment(month, totalJam, pembayaran);
            newPayment.setMahasiswa(mahasiswa);
            paymentRepository.save(newPayment);
        } else {
            payment.setTotalJam(payment.getTotalJam() + totalJam);
            payment.setPembayaran(payment.getPembayaran() + pembayaran);
            paymentRepository.save(payment);
        }
    }

    private Long[] getHourAndMonthDifferent(Long endtime, Long startTime) {
        long diffInMilis = endtime - startTime;
        long diffInHour = diffInMilis / (60 * 60 * 1000);
        long diffInMonths = diffInMilis / (30L * 24 * 60 * 60 * 1000);
        Long[] result = new Long[2];
        result[0] = diffInHour;
        result[1] = diffInMonths;

        return result;
    }

    private String getMonthName(int index) {
        String[] dfs = new DateFormatSymbols().getMonths();
        return dfs[index];
    }
}
