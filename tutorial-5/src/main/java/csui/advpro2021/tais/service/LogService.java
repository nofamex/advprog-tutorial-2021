package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;

public interface LogService {
    Log createLog(Log log, Mahasiswa mahasiswa);

    Log findLogById(Integer id);

    Iterable<Log> findAllLogByMahasiswa(Mahasiswa mahasiswa);

    Log updateLog(Integer id, Log log, Mahasiswa mahasiswa);

    Log deleteLog(Integer id);
}
