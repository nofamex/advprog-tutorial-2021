package csui.advpro2021.tais.service;

import csui.advpro2021.tais.model.Log;
import csui.advpro2021.tais.model.Mahasiswa;
import csui.advpro2021.tais.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class LogServiceImpl implements LogService {
    @Autowired
    private LogRepository logRepository;

    @Override
    public Log createLog(Log log, Mahasiswa mahasiswa) {
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log findLogById(Integer id) {
        Optional<Log> log = logRepository.findById(id);
        return log.orElseGet(() -> log.orElse(null));
    }

    @Override
    public Iterable<Log> findAllLogByMahasiswa(Mahasiswa mahasiswa) {
        return logRepository.findAllByMahasiswa(mahasiswa);
    }

    @Override
    public Log updateLog(Integer id, Log log, Mahasiswa mahasiswa) {
        log.setId(id);
        log.setMahasiswa(mahasiswa);
        logRepository.save(log);
        return log;
    }

    @Override
    public Log deleteLog(Integer id) {
        Log log = this.findLogById(id);
        if (log != null) {
            logRepository.delete(log);
        }
        return log;
    }
}
