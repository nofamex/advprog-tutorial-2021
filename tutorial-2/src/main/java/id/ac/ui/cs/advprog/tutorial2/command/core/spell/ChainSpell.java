package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import java.util.ArrayList;

public class ChainSpell implements Spell {
    private ArrayList<Spell> spells;

    public ChainSpell(ArrayList<Spell> spells) {
        this.spells = spells;
    }

    @Override
    public void cast() {
        for(Spell s: spells) {
            s.cast();
        }
    }

    @Override
    public void undo() {
        for(Spell s: spells) {
            s.undo();
        }
    }

    @Override
    public String spellName() {
        return "ChainSpell";
    }
}
