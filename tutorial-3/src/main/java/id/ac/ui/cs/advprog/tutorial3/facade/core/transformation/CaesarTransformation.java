package id.ac.ui.cs.advprog.tutorial3.facade.core.transformation;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.Codex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;

public class CaesarTransformation {

    private int shift;

    public CaesarTransformation(int shift) {
        this.shift = shift;
    }

    public CaesarTransformation() {
        this(5);
    }

    public Spell encode(Spell spell) {
        return process(spell, true);
    }

    public Spell decode(Spell spell) {
        return process(spell, false);
    }

    private Spell process(Spell spell, boolean encode) {
        int selector = encode ? 1 : -1;
        String text = spell.getText();
        Codex codex = spell.getCodex();
        char[] res = new char[text.length()];
        for (int i = 0; i < res.length; i++) {
            int oldCharAscii = selector > 0 ? (text.charAt(i) + shift) : (text.charAt(i) - shift);
            char newChar = (char) (oldCharAscii);
            res[i] = newChar;
        }
        return new Spell(new String(res), codex);
    }

}
