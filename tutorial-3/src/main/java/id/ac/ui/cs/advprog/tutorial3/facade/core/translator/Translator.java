package id.ac.ui.cs.advprog.tutorial3.facade.core.translator;

import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.AlphaCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.codex.RunicCodex;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.CodexTranslator;
import id.ac.ui.cs.advprog.tutorial3.facade.core.misc.Spell;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.AbyssalTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CaesarTransformation;
import id.ac.ui.cs.advprog.tutorial3.facade.core.transformation.CelestialTransformation;
import org.springframework.stereotype.Component;


@Component
public class Translator {
    private AbyssalTransformation abyssalTransformation;
    private CelestialTransformation celestialTransformation;
    private CaesarTransformation caesarTransformation;

    public Translator() {
        this.abyssalTransformation = new AbyssalTransformation();
        this.celestialTransformation = new CelestialTransformation();
        this.caesarTransformation = new CaesarTransformation();
    }

    public String encode(String text) {
        Spell spell = new Spell(text, AlphaCodex.getInstance());
        spell = celestialTransformation.encode(spell);
        spell = abyssalTransformation.encode(spell);
        spell = CodexTranslator.translate(spell, RunicCodex.getInstance());
        spell = caesarTransformation.encode(spell);
        return spell.getText();
    }

    public String decode(String code) {
        Spell spell = new Spell(code, RunicCodex.getInstance());
        spell = caesarTransformation.decode(spell);
        spell = CodexTranslator.translate(spell, AlphaCodex.getInstance());
        spell = abyssalTransformation.decode(spell);
        spell = celestialTransformation.decode(spell);
        return spell.getText();
    }
}
