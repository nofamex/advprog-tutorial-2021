package id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;

public class BowAdapter implements Weapon {

    private Bow bow;
    private boolean isAimShot;

    public BowAdapter(Bow bow) {
        this.bow = bow;
        this.isAimShot = false;
    }

    @Override
    public String normalAttack() {
        return bow.shootArrow(isAimShot);
    }

    @Override
    public String chargedAttack() {
        this.isAimShot = !isAimShot;
        if (isAimShot) {
            return "Entering aim shot mode";
        }
        return "Leaving aim shot mode";
    }

    @Override
    public String getName() {
        return bow.getName();
    }

    @Override
    public String getHolderName() {
        return bow.getHolderName();
    }
}
