package id.ac.ui.cs.advprog.tutorial3.adapter.service;

import id.ac.ui.cs.advprog.tutorial3.adapter.core.spellbook.Spellbook;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.bow.Bow;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weapon.Weapon;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.BowAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.core.weaponadapters.SpellbookAdapter;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.BowRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.LogRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.SpellbookRepository;
import id.ac.ui.cs.advprog.tutorial3.adapter.repository.WeaponRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class WeaponServiceImpl implements WeaponService {

    @Autowired
    private LogRepository logRepository;
    @Autowired
    private WeaponRepository weaponRepository;
    @Autowired
    private SpellbookRepository spellbookRepository;
    @Autowired
    private BowRepository bowRepository;

    @Override
    public List<Weapon> findAll() {
        List<Weapon> weaponList =  weaponRepository.findAll();
        List<Spellbook> spellbookList = spellbookRepository.findAll();
        List<Bow> bowList = bowRepository.findAll();

        for (Spellbook s: spellbookList) {
            SpellbookAdapter spellbookAdapter = new SpellbookAdapter(s);
            weaponList.add(spellbookAdapter);
            if (weaponRepository.findByAlias(s.getName()) == null) weaponRepository.save(spellbookAdapter);
        }
        for (Bow b: bowList) {
            BowAdapter bowAdapter = new BowAdapter(b);
            weaponList.add(bowAdapter);
            if (weaponRepository.findByAlias(b.getName()) == null) weaponRepository.save(bowAdapter);
        }

        return weaponList;
    }

    @Override
    public void attackWithWeapon(String weaponName, int attackType) {
        Weapon weapon = weaponRepository.findByAlias(weaponName);
        if (attackType == 1) {
            logRepository.addLog(weapon.getHolderName() + " uses " + weapon.getName() + " In Normal-Mode: " + weapon.normalAttack());
        } else {
            logRepository.addLog(weapon.getHolderName() + " uses " + weapon.getName() + " In Charge-Mode: " + weapon.chargedAttack());
        }
        weaponRepository.save(weapon);
    }

    @Override
    public List<String> getAllLogs() {
        return logRepository.findAll();
    }
}
