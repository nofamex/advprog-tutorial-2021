package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithGun implements AttackBehavior {

    @Override
    public String getType() {
        return "Attack With Gun";
    }

    @Override
    public String attack() {
        return "Attacking With Gun";
    }
}
