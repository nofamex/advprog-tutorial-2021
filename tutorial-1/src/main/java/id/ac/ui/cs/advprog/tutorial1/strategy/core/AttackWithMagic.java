package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {

    @Override
    public String getType() {
        return "Attack With Magic";
    }

    @Override
    public String attack() {
        return "Attacking With Magic";
    }
}
