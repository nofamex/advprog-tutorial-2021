#Perbedaan Eager Instantiation dan Lazy Instatiation
-Eager Instantiation menginstansiasi classnya sejak awal , jadi semenjak
aplikasi di build instancenya sudah ada dan siap dipakai
<br/>
-Lazy Instatiation menginstansiasi classnya tidak sejak awal, tetapi semenjak
method getInstancenya dipanggil barulah dia di instansiasi.

#Kelebihan
-Eager Instantiation tidak lagi memerlukan kita untuk memanggil methodnya, jadi kita
bisa langsung memanggil method dari class ini.
<br/>
-Lazy Instatiation akan menunggu dipanggil hinggal classnya dibuat dan ini bisa menghemat
penggunaan memory karena sedari awal dia akan null karena belum di instansiasi.

#Kekuarangan
-Eager Instantiation karena sedari awal sudah ada instancenya dia bisa memakan
memory lebih banyak dan membuat waktu build time lebih lama
<br/>
-Lazy Instantiation harus kita inisiasi dulu dan jika kita salah mengimplementasikan
penggunaan methodnya akan memungkinkan munculnya null.