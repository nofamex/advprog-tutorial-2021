package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.SnevnezhaShiratakiIngredientFactory;

public class SnevnezhaShirataki extends Menu {
    //Ingridients:
    //Noodle: Shirataki
    //Meat: Fish
    //Topping: Flower
    //Flavor: Umami
    private SnevnezhaShiratakiIngredientFactory factory;

    public SnevnezhaShirataki(String name){
        super(name);
        this.factory = new SnevnezhaShiratakiIngredientFactory();
        addIngredients();
    }

    @Override
    public void addIngredients() {
        super.setNoodle(this.factory.createNoodle());
        super.setMeat(this.factory.createMeat());
        super.setTopping(this.factory.createTopping());
        super.setFlavor(this.factory.createFlavour());
    }
}