package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.MondoUdonIngredientFactory;

public class MondoUdon extends Menu {
    //Ingridients:
    //Noodle: Udon
    //Meat: Chicken
    //Topping: Cheese
    //Flavor: Salty
    private MondoUdonIngredientFactory factory;

    public MondoUdon(String name) {
        super(name);
        this.factory = new MondoUdonIngredientFactory();
        addIngredients();
    }

    @Override
    public void addIngredients() {
        super.setNoodle(this.factory.createNoodle());
        super.setMeat(this.factory.createMeat());
        super.setTopping(this.factory.createTopping());
        super.setFlavor(this.factory.createFlavour());
    }
}