package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.InuzumaRamenIngredientFactory;

public class InuzumaRamen extends Menu {
    //Ingridients:
    //Noodle: Ramen
    //Meat: Pork
    //Topping: Boiled Egg
    //Flavor: Spicy
    private InuzumaRamenIngredientFactory factory;

    public InuzumaRamen(String name){
        super(name);
        this.factory = new InuzumaRamenIngredientFactory();
        addIngredients();
    }

    @Override
    public void addIngredients() {
        super.setNoodle(this.factory.createNoodle());
        super.setMeat(this.factory.createMeat());
        super.setTopping(this.factory.createTopping());
        super.setFlavor(this.factory.createFlavour());
    }
}