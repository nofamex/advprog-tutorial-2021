package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.flavor.Flavor;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.meat.Meat;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.noodle.Noodle;
import id.ac.ui.cs.advprog.tutorial4.factory.core.ingridients.topping.Topping;

public abstract class Menu {
    private String name;
    private Noodle noodle;
    private Meat meat;
    private Topping topping;
    private Flavor flavor;

    public Menu(String name){
        this.name = name;
    }

    public abstract void addIngredients();

    public String getName() {
        return name;
    }

    public Noodle getNoodle() {
        return noodle;
    }

    public Meat getMeat() {
        return meat;
    }

    public Topping getTopping() {
        return topping;
    }

    public Flavor getFlavor() {
        return flavor;
    }

    public void setNoodle(Noodle noodle) {
        this.noodle = noodle;
    }

    public void setMeat(Meat meat) {
        this.meat = meat;
    }

    public void setTopping(Topping topping) {
        this.topping = topping;
    }

    public void setFlavor(Flavor flavor) {
        this.flavor = flavor;
    }
}