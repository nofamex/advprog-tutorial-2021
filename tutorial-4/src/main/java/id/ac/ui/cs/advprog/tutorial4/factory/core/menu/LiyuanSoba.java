package id.ac.ui.cs.advprog.tutorial4.factory.core.menu;

import id.ac.ui.cs.advprog.tutorial4.factory.core.factory.LiyuanSobaIngredientFactory;

public class LiyuanSoba extends Menu {
    //Ingridients:
    //Noodle: Soba
    //Meat: Beef
    //Topping: Mushroom
    //Flavor: Sweet
    private LiyuanSobaIngredientFactory factory;

    public LiyuanSoba(String name){
        super(name);
        this.factory = new LiyuanSobaIngredientFactory();
        addIngredients();
    }

    @Override
    public void addIngredients() {
        super.setNoodle(this.factory.createNoodle());
        super.setMeat(this.factory.createMeat());
        super.setTopping(this.factory.createTopping());
        super.setFlavor(this.factory.createFlavour());
    }
}