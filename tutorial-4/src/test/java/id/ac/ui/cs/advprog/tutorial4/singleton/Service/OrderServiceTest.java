package id.ac.ui.cs.advprog.tutorial4.singleton.Service;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderService;
import id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.*;

public class OrderServiceTest {
    private Class<?> orderServiceClass;

    @Mock
    OrderDrink orderDrink;

    @Mock
    OrderFood orderFood;

    @InjectMocks
    OrderService orderService = new OrderServiceImpl();

    @BeforeEach
    public void setup() throws Exception {
        orderServiceClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.service.OrderServiceImpl");
        orderDrink = OrderDrink.getInstance();
        orderFood = OrderFood.getInstance();
    }

    @Test
    public void testOrderServiceHasOrderADrinkMethod() throws Exception {
        Method orderADrink = orderServiceClass.getDeclaredMethod("orderADrink", String.class);
        int methodModifiers = orderADrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceOrderADrinkMethodChangeTheDrink() {
        orderService.orderADrink("jus jambu");
        assertEquals(orderDrink.getDrink(), orderService.getDrink().getDrink());
    }

    @Test
    public void testOrderServiceHasGetDrinkMethod() throws Exception {
        Method getDrink = orderServiceClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceGetDrinkMethodReturnsDrinkName() {
        assertEquals(orderService.getDrink(), OrderDrink.getInstance());
    }

    @Test
    public void testOrderServiceHasOrderAFoodMethod() throws Exception {
        Method orderAFood = orderServiceClass.getDeclaredMethod("orderAFood", String.class);
        int methodModifiers = orderAFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceOrderAFoodMethodChangeTheFood() {
        orderService.orderAFood("nasi bakar");
        assertEquals(orderFood.getFood(), orderService.getFood().getFood());
    }
    @Test
    public void testOrderServiceHasGetFoodMethod() throws Exception {
        Method getFood = orderServiceClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderServiceGetFoodMethodReturnsFoodName() {
        assertEquals(orderService.getFood(), OrderFood.getInstance());
    }



}
