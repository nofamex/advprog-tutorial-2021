package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderDrinkTest {
    private Class<?> orderDrinkClass;
    private OrderDrink orderDrink;

    @Mock
    private String drink;

    @BeforeEach
    public void setup() throws Exception {
        drink = "jus melon";
        orderDrinkClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderDrink");
        orderDrink = (OrderDrink) Mockito.mock(orderDrinkClass, Mockito.CALLS_REAL_METHODS);
        ReflectionTestUtils.setField(orderDrink, "drink", drink);
    }

    @Test
    public void testOrderDrinkIsAPublicClass() {
        int classModifiers = orderDrinkClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testOrderDrinkHasGetInstanceMethod() throws Exception {
        Method getInstance = orderDrinkClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isStatic(methodModifiers));
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkGetInstanceMethodIsSingleton() {
        assertEquals(OrderDrink.getInstance(), OrderDrink.getInstance());
    }

    @Test
    public void testOrderDrinkHasGetDrinkMethod() throws Exception {
        Method getDrink = orderDrinkClass.getDeclaredMethod("getDrink");
        int methodModifiers = getDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkGetDrinkMethodReturnDrinkName() {
        assertEquals(orderDrink.getDrink(), "jus melon");
    }

    @Test
    public void testOrderDrinkHasSetDrinkMethod() throws Exception {
        Method setDrink = orderDrinkClass.getDeclaredMethod("setDrink", String.class);
        int methodModifiers = setDrink.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkSetDrinkChangesTheDrink() {
        orderDrink.setDrink("jus apel");
        assertEquals(orderDrink.getDrink(), "jus apel");
    }

    @Test
    public void testOrderDrinkHasToStringMethod() throws Exception {
        Method toString = orderDrinkClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderDrinkToStringMethodReturnDrinkName() {
        assertEquals(orderDrink.toString(), orderDrink.getDrink());
    }
}
