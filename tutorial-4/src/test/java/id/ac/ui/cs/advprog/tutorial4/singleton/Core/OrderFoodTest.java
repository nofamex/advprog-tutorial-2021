package id.ac.ui.cs.advprog.tutorial4.singleton.Core;

import id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.test.util.ReflectionTestUtils;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class OrderFoodTest {
    public Class<?> orderFoodClass;
    public OrderFood orderFood;

    @Mock
    private String food;

    @BeforeEach
    public void setup() throws Exception {
        food = "nasi goreng";
        orderFoodClass = Class.forName("id.ac.ui.cs.advprog.tutorial4.singleton.core.OrderFood");
        orderFood = (OrderFood) Mockito.mock(orderFoodClass, Mockito.CALLS_REAL_METHODS);
        ReflectionTestUtils.setField(orderFood, "food", food);
    }

    @Test
    public void testOrderFoodIsAPublicClass() {
        int classModifiers = orderFoodClass.getModifiers();
        assertTrue(Modifier.isPublic(classModifiers));
    }

    @Test
    public void testOrderFoodHasGetInstanceMethod() throws Exception {
        Method getInstance = orderFoodClass.getDeclaredMethod("getInstance");
        int methodModifiers = getInstance.getModifiers();
        assertTrue(Modifier.isStatic(methodModifiers));
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodGetInstanceMethodIsSingleton() {
        assertEquals(OrderFood.getInstance(), OrderFood.getInstance());
    }

    @Test
    public void testOrderFoodHasGetFoodMethod() throws Exception {
        Method getFood = orderFoodClass.getDeclaredMethod("getFood");
        int methodModifiers = getFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }
    @Test
    public void testOrderFoodGetFoodMethodReturnFoodName() {
        assertEquals(orderFood.getFood(), "nasi goreng");
    }

    @Test
    public void testOrderFoodHasSetFoodMethod() throws Exception {
        Method setFood = orderFoodClass.getDeclaredMethod("setFood", String.class);
        int methodModifiers = setFood.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodSetFoodChangesTheFood() {
        orderFood.setFood("nasi rames");
        assertEquals(orderFood.getFood(), "nasi rames");
    }

    @Test
    public void testOrderFoodHasToStringMethod() throws Exception {
        Method toString = orderFoodClass.getDeclaredMethod("toString");
        int methodModifiers = toString.getModifiers();
        assertTrue(Modifier.isPublic(methodModifiers));
    }

    @Test
    public void testOrderFoodToStringMethodReturnFoodName() {
        assertEquals(orderFood.toString(), orderFood.getFood());
    }
}
